# Web Crawler and Web Scraping
## with Nodejs Puppeteer

### Target : https://www.tutors.com

* Projeto desenvolvido por Cleiton Corrêa a fins de avaliação

![Web Scrapping Developer By Cleiton Correa](https://analyticsindiamag.com/wp-content/uploads/2020/12/Puppeteer.jpg) 


```
npm install

```
 - link de login https://tutors.com/login
-- link das notificações : https://tutors.com/pros/quotes

login

Request URL: https://tutors.com/login
Request Method: POST
Status Code: 200 
Remote Address: [2606:4700:3108::ac42:2b87]:443
Referrer Policy: strict-origin-when-cross-origin

--headers login
:authority: tutors.com
:method: POST
:path: /login
:scheme: https
accept: */*
accept-encoding: gzip, deflate, br
accept-language: pt-BR,pt;q=0.9
content-length: 124
content-type: application/x-www-form-urlencoded; charset=UTF-8
cookie: _csrf=pEEIZCDTaovLkW1kp5MFbfRU; day_session_token=s%3AeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NTE4ODIwMTR9.JKRjeoe8FmTAhhkO3G6HrqtHofbLHnOq-VAd44Pn1ms.cCm87%2BXCzQaCAiV99A8N43Y2MrJSKPHrAH8uuRd9GZQ; mp_770b0ded5c3c0a26f20621883f4df33a_mixpanel=%7B%22distinct_id%22%3A%20%221809bd739074d6-03a417b25dda7d-6b3e555b-e1000-1809bd73908c86%22%2C%22%24device_id%22%3A%20%221809bd739074d6-03a417b25dda7d-6b3e555b-e1000-1809bd73908c86%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; session_token=s%3AeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NTE4ODIwMTR9.JKRjeoe8FmTAhhkO3G6HrqtHofbLHnOq-VAd44Pn1ms.cCm87%2BXCzQaCAiV99A8N43Y2MrJSKPHrAH8uuRd9GZQ; _ga=GA1.2.1204080382.1651882015; _gid=GA1.2.151286405.1651882015
origin: https://tutors.com
referer: https://tutors.com/login
sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"
sec-ch-ua-mobile: ?0
sec-ch-ua-platform: "Windows"
sec-fetch-dest: empty
sec-fetch-mode: cors
sec-fetch-site: same-origin
user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36
x-requested-with: XMLHttpRequest

---Payload
_csrf: pfJxF4uJ-fDar7QrAgcKCfz1zNJz7tUtcuSU
users[email]: cleitoncorreadesigner@gmail.com
users[password]: ####

-- Obter lista de Jobs
url: https://tutors.com/pros/requests
