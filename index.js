const puppeteer = require("puppeteer");

(async() => {
    const browserChrome = await puppeteer.launch({ headless: false });
    const pageChrome = await browserChrome.newPage();
    await pageChrome.goto("https://www.tutors.com/login");
    await pageChrome.waitForSelector(".login-box clearfix");
    await pageChrome.click(".input ico-email");
    await pageChrome.waitForSelector(".username");
    await pageChrome.type(".username", "#######@####", {
        delay: 300,
    });
    await pageChrome.type(".password", "#####", {
        delay: 300,
    });
    await pageChrome.click("#loginButton");

    console.log(pageChrome);
    await browserChrome.close();
})();

// const creds = { email: "myemail", password: "mypassword", };
// (async() = {

//     browser = await puppeteer.launch({ headless: false, args: ['--start-maximized'] })
//     page = await browser.newPage();
//     await page.setViewport({ width: 1920, height: 1080 })
//     await page.goto('https://tutors.com/index.html', { waitUntil: 'networkidle0' })

//     const username = await page.$x('/html/body/div[1]/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div[3]/div/div[1]/div/div/div/div/div/div[2]/div/div[1]/div/input[1]')
//     await username[0].click()
//     await page.waitForNavigation({ waitUntil: 'networkidle0' })
//     await page.type(username, creds.email);
//     await page.waitForNavigation({ waitUntil: 'networkidle0' })
//     await browser.close()
// })();
