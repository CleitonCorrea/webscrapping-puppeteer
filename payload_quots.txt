payload quots

<div id="template-content">
<div class="pro-requests">
		

<!-- <div class="requests-holder row"> -->

		<article class="request-box" id="6275af8fd4350f7f87e7eda2" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="62757e958c982d3d8409a9fc">
			<a href="/pros/quote/N1xnAqRHh/Ek83aPAB2" class="request-head">
				<div class="unread-lead "></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Martine S. <!-- contacted 1 pro --> (4 hr ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/N1xnAqRHh/Ek83aPAB2" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/N1xnAqRHh/Ek83aPAB2" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">Davenport, FL 33897
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">	
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">							
								<div class="request-tag">14 - 18 years old</div><div class="request-tag">General improvement</div><div class="request-tag">Improving task completion</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">Wednesday</div><div class="request-tag">Thursday</div><div class="request-tag">Friday</div><div class="request-tag">Morning (9am - noon)</div><div class="request-tag">Early afternoon (noon - 3pm)</div><div class="request-tag">Late afternoon (3 - 6pm)</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">As soon as possible</div><div class="request-tag">Conducting initial research</div><div class="request-tag">The tutor travels to me</div><div class="request-tag">The student has ADD</div><div class="request-tag"> enrolled in grad academy</div><div class="request-tag"> and is failing to complete 10 online assignments a day as required. .</div>					
					</div>
				</div>
			</a>
		</article>
		<article class="request-box" id="6275af8cd4350f7f87e7eda0" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="62753fa3909b473d7eb4e4d0">
			<a href="/pros/quote/4JpsAcRHh/EJVTAmAS3" class="request-head">
				<div class="unread-lead blue"></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Amy L. <!-- contacted 1 pro --> (8 hr ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/4JpsAcRHh/EJVTAmAS3" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/4JpsAcRHh/EJVTAmAS3" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">St. Petersburg, FL 33704
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">	
							I travel to the tutor's location - 15 miles,The tutor travels to me,<div class="hidden">15 miles</div>						
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">			
						
						
						
							
								
							
								
							
								
							
								
							
								
	
							
								<div class="request-tag">14 - 18 years old</div><div class="request-tag">Prepare for a test</div><div class="request-tag">Improve grade</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">Do well on upcoming test </div><div class="request-tag">Monday</div><div class="request-tag">Tuesday</div><div class="request-tag">Wednesday</div><div class="request-tag">Thursday</div><div class="request-tag">Friday</div><div class="request-tag">Saturday</div><div class="request-tag">Sunday</div><div class="request-tag">Late afternoon (3 - 6pm)</div><div class="request-tag">Evening (after 6pm)</div><div class="request-tag">60 minutes</div><div class="request-tag">As soon as possible</div><div class="request-tag">Need some more information before hiring</div><div class="request-tag">I travel to the tutor's location - 15 miles</div><div class="request-tag">The tutor travels to me</div><div class="request-tag"><div class="hidden">15 miles</div></div><div class="request-tag">.</div>
						
					
					</div>
				</div>
			</a>
		</article>

		<article class="request-box" id="6275af87d4350f7f87e7ed9e" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="627494288c982d3d8409a62a">
			<a href="/pros/quote/41djRc0rn/EJFH7KpB3" class="request-head">
				<div class="unread-lead blue"></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Jay S. <!-- contacted 1 pro --> (13 hr ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/41djRc0rn/EJFH7KpB3" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/41djRc0rn/EJFH7KpB3" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">Tampa, FL 33616
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">
					
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">							
								<div class="request-tag">14 - 18 years old</div><div class="request-tag">First 2 </div><div class="request-tag">Get a better understanding of the topics I'm currently struggling on to better prepare for a final exam</div><div class="request-tag">Just 1 time </div><div class="request-tag">Maybe 1-2 hours </div><div class="request-tag">Friday</div><div class="request-tag">Saturday</div><div class="request-tag">Sunday</div><div class="request-tag">Morning (9am - noon)</div><div class="request-tag">More than 90 minutes</div><div class="request-tag">As soon as possible</div><div class="request-tag">Ready to hire the right tutor today</div><div class="request-tag">Online tutoring</div><div class="request-tag">I have to use my personal hotspot so connect might not be great if calling on skype or anything like that .</div>
						
					
					</div>
				</div>
			</a>
		</article>

	
		

		<article class="request-box" id="6275af85d4350f7f87e7ed9c" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="627471dad9ff763d2aa64dff">
			<a href="/pros/quote/NyLsA90H3/EJjxWDprn" class="request-head">
				<div class="unread-lead blue"></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Andrew G. <!-- contacted 1 pro --> (23 hr ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/NyLsA90H3/EJjxWDprn" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/NyLsA90H3/EJjxWDprn" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">Valrico, FL 33594
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">
					
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							I travel to the tutor's location - 15 miles,The tutor travels to me,Online tutoring,<div class="hidden">15 miles</div>
						
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">
					

						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								<div class="request-tag">18 or older</div><div class="request-tag">Prepare for a test</div><div class="request-tag"></div><div class="request-tag">As recommended by tutor</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">Monday</div><div class="request-tag">Tuesday</div><div class="request-tag">Wednesday</div><div class="request-tag">Thursday</div><div class="request-tag">Friday</div><div class="request-tag">Saturday</div><div class="request-tag">Sunday</div><div class="request-tag">Late afternoon (3 - 6pm)</div><div class="request-tag">Evening (after 6pm)</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">As soon as possible</div><div class="request-tag">Conducting initial research</div><div class="request-tag">I travel to the tutor's location - 15 miles</div><div class="request-tag">The tutor travels to me</div><div class="request-tag">Online tutoring</div><div class="request-tag"><div class="hidden">15 miles</div></div><div class="request-tag">.</div>
						
					
					</div>
				</div>
			</a>
		</article>

	
		

		<article class="request-box" id="6275af85d4350f7f87e7ed9a" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="627470a2c89d493d2b27da73">
			<a href="/pros/quote/41xBsRqRBn/Vy7pkw6Sh" class="request-head">
				<div class="unread-lead blue"></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Brinda S. <!-- contacted 1 pro --> (23 hr ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/41xBsRqRBn/Vy7pkw6Sh" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/41xBsRqRBn/Vy7pkw6Sh" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">TWN N CNTRY, FL 33615
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">
					
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							The tutor travels to me
						
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">
					

						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								<div class="request-tag">11 - 13 years old</div><div class="request-tag">General improvement</div><div class="request-tag"></div><div class="request-tag">More than once a week</div><div class="request-tag">On an ongoing basis</div><div class="request-tag">Tuesday</div><div class="request-tag">Wednesday</div><div class="request-tag">Thursday</div><div class="request-tag">Friday</div><div class="request-tag">Evening (after 6pm)</div><div class="request-tag">30 - 45 minutes</div><div class="request-tag">As soon as possible</div><div class="request-tag">Ready to hire the right tutor today</div><div class="request-tag">The tutor travels to me</div><div class="request-tag">.</div>
						
					
					</div>
				</div>
			</a>
		</article>

	
		

		<article class="request-box" id="6275af84d4350f7f87e7ed98" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="62747003c89d493d2b27da66">
			<a href="/pros/quote/VySsR9RB2/4kEQJwaBn" class="request-head">
				<div class="unread-lead blue"></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Carrie B. <!-- contacted 1 pro --> (23 hr ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/VySsR9RB2/4kEQJwaBn" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/VySsR9RB2/4kEQJwaBn" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">Fort Myers, FL 33908
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">
					
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							I travel to the tutor's location - 10 miles,The tutor travels to me,<div class="hidden">10 miles</div>
						
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">
					

						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								<div class="request-tag">5 - 10 years old</div><div class="request-tag">General improvement</div><div class="request-tag"></div><div class="request-tag">Once a week</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">Tuesday</div><div class="request-tag">Thursday</div><div class="request-tag">Friday</div><div class="request-tag">Saturday</div><div class="request-tag">Early morning (before 9am)</div><div class="request-tag">Morning (9am - noon)</div><div class="request-tag">Late afternoon (3 - 6pm)</div><div class="request-tag">Evening (after 6pm)</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">As soon as possible</div><div class="request-tag">Ready to hire the right tutor today</div><div class="request-tag">I travel to the tutor's location - 10 miles</div><div class="request-tag">The tutor travels to me</div><div class="request-tag"><div class="hidden">10 miles</div></div><div class="request-tag">.</div>
						
					
					</div>
				</div>
			</a>
		</article>

	
		

		<article class="request-box" id="6275af83d4350f7f87e7ed96" data-provider-id="6275aee5d9ff763d2aa657db" data-lead-id="627467a4909b473d7eb4e36a">
			<a href="/pros/quote/4JEjRq0Sn/NyB6LUpBh" class="request-head">
				<div class="unread-lead blue"></div>
				<h2>Math Tutoring</h2>
				<p class="mb8">Curtis D. <!-- contacted 1 pro --> (1 day ago) <!-- <strong>&middot;</strong> 1 pro responded --></p>
				<div class="hidden">
					<div class="pill pill--green">Be the first</div>
				</div>
			</a>
			<div class="request-cta">
				<button type="button" class="btn-pass " data-toggle="modal" data-target="#pass-reason"><span class="toggle-pass-reason-modal">Pass</span></button>
				<a href="/pros/quote/4JEjRq0Sn/NyB6LUpBh" class="btn-viewlead">View Details</a>
			</div>
			<a href="/pros/quote/4JEjRq0Sn/NyB6LUpBh" class="request-body">
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_location_dark.png">Jacksonville, FL 32259
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_calendar_dark.png">
					
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							I travel to the tutor's location - 15 miles,The tutor travels to me,<div class="hidden">15 miles</div>
						
				</div>
				<div class="request-item">
					<img src="//cdn.homeguide.com/assets/images/email/icon_information.png">
					<div class="request-tag-box">
					

						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
							
						
						
						
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
								<div class="request-tag">11 - 13 years old</div><div class="request-tag">Prepare for a test</div><div class="request-tag">Prepare for FSA test and become more comfortable with math.</div><div class="request-tag">More than once a week</div><div class="request-tag">As recommended by tutor</div><div class="request-tag">Wednesday</div><div class="request-tag">Friday</div><div class="request-tag">Saturday</div><div class="request-tag">Sunday</div><div class="request-tag">Late afternoon (3 - 6pm)</div><div class="request-tag">Evening (after 6pm)</div><div class="request-tag">60 minutes</div><div class="request-tag">As soon as possible</div><div class="request-tag">Need some more information before hiring</div><div class="request-tag">I travel to the tutor's location - 15 miles</div><div class="request-tag">The tutor travels to me</div><div class="request-tag"><div class="hidden">15 miles</div></div><div class="request-tag">8th grade Algebra.</div>
						
					
					</div>
				</div>
			</a>
		</article>

	




</div>
<!-- <li class="related-requests hidden">Additional requests to explore<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Marking yourself as hired also increases your number of hired jobs. Customers have more confidence to hire pros with more reviews and hired jobs." style="color:#666;"><i class="fa fa-question-circle fa-fw"></i></span></li> -->


		<p class="body-text pro-task-change">
			Not the right type of requests?
			<a href="/pros/settings#request_types">Change your service preferences.</a>
		</p>
	
	<!-- <p>Need help? <a href="javascript:zend();">Contact us</a></p> -->



<div id="messageTips" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="opacity: 1;color:#dcdcdc;font-size: 30px;padding: 5px 12px;position: absolute;z-index: 1;right: 0;"><span aria-hidden="true">×</span></button>
			<div class="modal-header">

				<h5 class="modal-title">Additional Requests</h5>
			</div>

			<div class="modal-body">
				<p class="sub-header">
					We didn’t notify you of these requests since we weren’t confident you’d be interested. When you quote on these additional requests, we’ll send you more requests like these in the future.
				</p>
				
				<p class="navigation">
					<button class="btn btn-white btn-xl btn-text" type="button" data-dismiss="modal">OK</button>
				</p>

			</div>

		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).on('click', '.related-requests i', function(){
		$('#messageTips').modal('show');
	})
</script>

<script type="text/javascript">
	if(~url.indexOf('/pros/requests?intro')) 
		$(document).ready( function() {
		  $('.empty-state.normal').addClass( 'hidden' );
		  $('.empty-state.intro').removeClass( 'hidden' );
		});
</script></div>